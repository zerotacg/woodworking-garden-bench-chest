$s = 0.5;
bench_width = 1400;
bench_depth_boards = 5;
bench_height_boards = 5;

board_size = 19;
board_width = 95;
board_gap = 6;
lid_gap = 5;

pole_size = 45;
pole_width = 75;
pole_height = bench_height_boards * (board_gap + board_width) - pole_size;
hole_offset = [pole_width / 2 + board_size, 20];
hole_offset_top = pole_size + 20;

bench_depth = bench_depth_boards * board_width + (bench_depth_boards - 1) * board_gap;
bench_height = bench_height_boards * (board_gap + board_width);

pole_inset = board_size + lid_gap;
pole_depth = bench_depth - 2 * pole_inset;

main();

module main() {
    front();
    
    translate([bench_depth,0,0])
    mirror([1,0,0])
    front();

    translate([0,0,2*$s])
    translate([0,0,bench_height])
    lid();
}

module front() {
    translate([0,0,board_gap])
    difference() {
        union() {
            for (i = [0:bench_height_boards - 2]) {
                translate([0,0,i * (board_width + board_gap)])
                boardFrontBottom();
            }

            translate([0,0,(bench_height_boards - 1) * (board_width + board_gap)])
            boardFrontTop();
        }
    }
    
    translate([0,board_size,0])
    poleSide();

    translate([0,bench_width - pole_width,0])
    translate([0,-board_size,0])
    poleSide();

    translate([0,(bench_width - pole_width) / 2,0])
    poleSide();
}

module boardFrontTop() {
    difference() {
        boardFront();
        
        spreadHoles() {
            translate([0,0,hole_offset[1]])
            hole();

            translate([0,0,board_width - hole_offset_top])
            hole();
        }
    }
}

module boardFrontBottom() {
    difference() {
        boardFront();
        
        spreadHoles() {
            holesSide();
        }
    }
}

module spreadHoles() {
    translate([0, hole_offset[0], 0]) {
        children(); 
    }

    translate([0, bench_width - hole_offset[0], 0]) {
        children(); 
    }

    translate([0, bench_width / 2, 0]) {
        children(); 
    }
}

module holesSide() {
    translate([0,0,hole_offset[1]])
    hole();

    translate([0,0,board_width - hole_offset[1]])
    hole();
}

module boardFront() {
    cube([board_size, bench_width, board_width]);
}

module hole() {
    depth = 2*$s + board_size;
    diameter = 4;

    translate([-$s,0,0])
    rotate([0,90,0])
    cylinder(h=depth, d=diameter);
}

module poleSide() {
    translate([$s,0,0])
    translate([board_size,0,0])
    cube([pole_size, pole_width, pole_height]);
}


module lid() {
    for (i = [0:bench_depth_boards - 1]) {
        translate([i * (board_width + board_gap),0,0])
        boardLid();
    }
    
    translate([0,board_size,0])
    poleLid();

    translate([0,bench_width - pole_width,0])
    translate([0,-board_size,0])
    poleLid();

    translate([0,(bench_width - pole_width)/2,0])
    poleLid();
}

module boardLid() {
    cube([board_width, bench_width, board_size]);
}

module poleLid() {
    translate([0,0,-$s])
    translate([0,0,-pole_size])
    translate([pole_inset,0,0])
    cube([pole_depth, pole_width, pole_size]);
}
